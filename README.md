## View Mode

View mode provides a field type to store view modes.

This can be used to allow users to pick from a set of preselected
view modes, and is best integrated with Display Suite or to provide
custom logic based on user-selected view modes.

This provides an alternative to storing view modes in an options
field that cannot be changed after the field contains data.

