<?php

namespace Drupal\view_mode\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Plugin implementation of the 'view_mode_select_widget' widget.
 *
 * @FieldWidget(
 *   id = "view_mode_select_widget",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "list_view_mode"
 *   }
 * )
 */
class ViewModeSelectWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    $value = isset($items[$delta]->value) ? $items[$delta]->value : '';

    $element += [
      '#type' => 'select',
      '#options' => $this->getOptions($items->getEntity()),
      '#empty_option' => $this->t('- Select View Mode -'),
      '#default_value' => $value,
    ];

    return ['value' => $element];
  }

  /**
   * Returns the array of options for the widget.
   *
   * @return array
   *   The array of options for the widget.
   */
  protected function getOptions(FieldableEntityInterface $entity) {
    // Limit the settable options for the current user account.
    $options = $this->fieldDefinition
      ->getFieldStorageDefinition()
      ->getOptionsProvider('value', $entity)
      ->getSettableOptions();

    return $options;
  }

}
