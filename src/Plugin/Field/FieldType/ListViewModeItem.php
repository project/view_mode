<?php

namespace Drupal\view_mode\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\OptionsProviderInterface;

/**
 * Plugin implementation of the 'list_view_mode' field type.
 *
 * @FieldType(
 *   id = "list_view_mode",
 *   label = @Translation("View mode"),
 *   description = @Translation("This field stores view modes"),
 *   default_widget = "view_mode_select_widget",
 *   default_formatter = "view_mode_default_formatter"
 * )
 */
class ListViewModeItem extends FieldItemBase implements OptionsProviderInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'type' => 'varchar',
          'length' => '255',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('View mode'))
      ->addConstraint('Length', ['max' => 255])
      ->setRequired(TRUE);
    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleOptions(AccountInterface $account = NULL): array {
    $options = $this->getOptions();
    $view_modes = $this->getSetting('view_modes');

    $possible_options = [];
    foreach ($options as $entity_type => $data) {
      foreach ($data as $key => $value) {
        if (isset($view_modes[$key])) {
          $possible_options[$key] = $value;
          unset($options[$key]);
        }
      }
    }

    return $possible_options;
  }

  /**
   * {@inheritdoc}
   */
  public function getPossibleValues(AccountInterface $account = NULL): array {
    return array_keys($this->getPossibleOptions($account));
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableValues(AccountInterface $account = NULL): array {
    return $this->getPossibleValues($account);
  }

  /**
   * {@inheritdoc}
   */
  public function getSettableOptions(AccountInterface $account = NULL): array {
    return $this->getPossibleOptions($account);
  }

  /**
   * Return view modes that can be selected. This depends on the entity this
   * field is part of, currently no view modes from other entity types are
   * selectable.
   *
   * @param string $entity_type
   *
   * @return array|mixed [type] [description]
   */
  protected function getOptions(string $entity_type = '') {

    /** @var \Drupal\Core\Entity\EntityDisplayRepository $entityDisplayRepository */
    $entityDisplayRepository = \Drupal::service('entity_display.repository');

    if ($entity_type) {
      $view_modes_info[$entity_type] = $entityDisplayRepository->getViewModeOptions($entity_type);
    }
    else {
      $view_modes_info = $entityDisplayRepository->getAllViewModes();
    }

    $options = [];
    foreach ($view_modes_info as $type => $data) {
      foreach ($data as $view_mode_name => $view_mode_info) {
        $options[$type][$view_mode_name] = $view_mode_info['label'];
      }
    }

    if ($entity_type) {
      return $options[$entity_type];
    }
    else {
      return $options;
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultFieldSettings(): array {
    return [
        'view_modes' => [],
      ] + parent::defaultFieldSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function fieldSettingsForm(array $form, FormStateInterface $form_state): array {
    $element = [];
    $flatten_options = [];

    $options = $this->getOptions();
    foreach ($options as $entity_type => $data) {
      foreach ($data as $key => $value) {
        $flatten_options[$key] = $value;
      }
    }

    $element['view_modes'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Enabled view modes'),
      '#description' => $this->t('Select the view modes that can be selected for this field.'),
      '#default_value' => $this->getSetting('view_modes'),
      '#options' => $flatten_options,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function fieldSettingsToConfigData(array $settings): array {
    foreach ($settings['view_modes'] as $key => $status) {
      if (!$status) {
        unset($settings['view_modes'][$key]);
      }
    }
    return $settings;
  }

}
